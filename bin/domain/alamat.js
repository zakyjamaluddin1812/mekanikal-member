const { InternalServerError, UnprocessableEntityError, UnauthorizedError, NotFoundError } = require('../helper/error')
const AlamatModel = require('../infrastructure/repositories/alamat')

const alamatModel = new AlamatModel()

class Alamat {

  async getWilayah(kode) {
    if(kode == null || kode == "") {
      const data = await this.getProvinsi()
      const message = "provinsi"
      return {data, message}
    } 
    if(kode.length == 2) {
      const data = await this.getKabupaten(kode)
      const message = "kabupaten / kota"
      return {data, message}
    }
    if(kode.length == 4) {
      const data = await this.getKecamatan(kode)
      const message = "kecamatan"
      return {data, message}
    }
    else {

    }

  }
    
  async getProvinsi() {
    const provinsi = await alamatModel.getProvinsi();
    if(provinsi.err || !provinsi.data) {
      return new InternalServerError('fail to get provinsi')
    }    
    return provinsi.data
  }

  async getKabupaten(kode) {
    const kabupaten = await alamatModel.getKabupaten(kode);
    if(kabupaten.err || !kabupaten.data) {
      return new InternalServerError('fail to get kabupaten')
    }    
    return kabupaten.data
  }

  async getKecamatan(kode) {
    const kecamatan = await alamatModel.getKecamatan(kode);
    if(kecamatan.err || !kecamatan.data) {
      return new InternalServerError('fail to get kecamatan')
    }    
    return kecamatan.data

  }
}

module.exports = Alamat