const { InternalServerError, UnprocessableEntityError, UnauthorizedError, NotFoundError } = require('../helper/error')
const PartnerModel = require('../infrastructure/repositories/partner')
const { unflatten } = require('flat')
var dateFormat = require("commonjs-date-formatting").strftime;
const bcrypt = require("bcrypt");

const partnerM = new PartnerModel()

class Partner {
  async get(paging) {
    const {page, size} = paging
    const skip = (page - 1) * size
    const {collection, totalData} = await partnerM.get(skip, parseInt(size));
    if(collection.err || !collection.data) {
      return new InternalServerError(`fail to get partner, ${collection.err}`)
    }
    const data = collection.data.map(item => unflatten({
      ...item,
      aktif : item['aktif'] == 1 ? true : false,

    }))
    return {
      data,
      meta : {
        page, size, totalData,
        totalDataOnPage : collection.data.length
      }
    }
  }

  async getLokasi() {
    const partner = await partnerM.getLokasi();
    if(partner.err || !partner.data) {
      return new InternalServerError(`fail to get lokasi partner, ${partner.err}`)
    }
    // const data = partner.data.map(item => unflatten({
    //   ...item,
    //   aktif : item['aktif'] == 1 ? true : false,

    // }))
    return partner.data
  }

  async find(id) {
    const partner = await partnerM.find(id);
    if(partner.err || !partner.data) {
      return new InternalServerError(`fail to add partner, ${partner.err}`)
    }
    const data = partner.data.map(item => unflatten({
      ...item,
      aktif : item['aktif'] == 1 ? true : false,

    }))
    return data[0]
  }

  async add(payload) {
    payload.aktif = payload.aktif == true ? 1 : 0
    const partner = await partnerM.add(payload);
    if(partner.err || !partner.data) {
      return new InternalServerError(`fail to add partner, ${partner.err}`)
    }
    return null
  }

  async update(payload, id) {
    payload.aktif = payload.aktif == true ? 1 : 0
    const partner = await partnerM.update(payload, id);
    if(partner.err || !partner.data) {
      return new InternalServerError(`fail to edit partner, ${partner.err}`)
    }
    return null

  }

  async delete(id) {
    const partner = await partnerM.delete(id);
    if(partner.err || !partner.data) {
      return new InternalServerError(`fail to delete partner, ${partner.err}`)
    }
    return null
  }
}

module.exports = Partner