const { InternalServerError, UnprocessableEntityError, UnauthorizedError, NotFoundError } = require('../helper/error')
const StatusMekanikModel = require('../infrastructure/repositories/statusMekanik')

const statusMekanikM = new StatusMekanikModel()

class statusMekanik {
  async get() {
    const statusMekanik = await statusMekanikM.get();
    if(statusMekanik.err || !statusMekanik.data) {
      return new InternalServerError(`fail to get statusMekanik, ${statusMekanik.err}`)
    }
    return statusMekanik.data
  }

  async find(id) {
    const statusMekanik = await statusMekanikM.find(id);
    if(statusMekanik.err || !statusMekanik.data) {
      return new InternalServerError(`fail to find statusMekanik, ${statusMekanik.err}`)
    }
    return statusMekanik.data[0]
  }

  async add(payload) {
    const statusMekanik = await statusMekanikM.add(payload);
    if(statusMekanik.err || !statusMekanik.data) {
      return new InternalServerError(`fail to add statusMekanik, ${statusMekanik.err}`)
    }
    return null
  }

  async update(payload, id) {
    const statusMekanik = await statusMekanikM.update(payload, id);
    if(statusMekanik.err || !statusMekanik.data) {
      return new InternalServerError(`fail to edit statusMekanik, ${statusMekanik.err}`)
    }
    return null

  }

  async delete(id) {
    const statusMekanik = await statusMekanikM.delete(id);
    if(statusMekanik.err || !statusMekanik.data) {
      return new InternalServerError(`fail to delete statusMekanik, ${statusMekanik.err}`)
    }
    return null
  }
}

module.exports = statusMekanik