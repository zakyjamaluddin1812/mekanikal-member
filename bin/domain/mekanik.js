const { InternalServerError, UnprocessableEntityError, UnauthorizedError, NotFoundError } = require('../helper/error')
const MekanikModel = require('../infrastructure/repositories/mekanik')
const { unflatten } = require('flat')
var dateFormat = require("commonjs-date-formatting").strftime;
const bcrypt = require("bcrypt");

const mekanikM = new MekanikModel()

class Mekanik {
  async get(query) {
    const {page, size, word, location, status} = query
    const skip = (page - 1) * size
    const {collection, totalData} = await mekanikM.get(skip, parseInt(size), word, location, status);
    if(collection.err || !collection.data) {
      return new InternalServerError(`fail to get mekanik, ${collection.err}`)
    }
    const data = collection.data.map(item => unflatten({
      ...item,
      aktif : item['aktif'] == 1 ? true : false,
      tanggalLahir : dateFormat(item['tanggalLahir'], "%d %B %Y")
    }))
    return {
      data,
      meta : {
        page, size, totalData,
        totalDataOnPage : collection.data.length
      }
    }
  }

  async find(id) {
    const mekanik = await mekanikM.find(id);
    if(mekanik.err || !mekanik.data) {
      return new InternalServerError(`fail to add mekanik, ${mekanik.err}`)
    }
    const data = mekanik.data.data.map(item => unflatten({
      ...item,
      aktif : item['aktif'] == 1 ? true : false,
      tanggalLahir : dateFormat(item['tanggalLahir'], "%d %B %Y")
    }))
    return data[0]
  }

  async add(payload) {
    payload.aktif = payload.aktif == true ? 1 : 0
    const salt = await bcrypt.genSalt(10);
    payload.sandi = await bcrypt.hash(payload.sandi, salt)
    const mekanik = await mekanikM.add(payload);
    if(mekanik.err || !mekanik.data) {
      return new InternalServerError(`fail to add mekanik, ${mekanik.err}`)
    }
    delete payload.sandi
    return null
  }

  async update(payload, id) {
    payload.aktif = payload.aktif == true ? 1 : 0
    const salt = await bcrypt.genSalt(10);
    payload.sandi = await bcrypt.hash(payload.sandi, salt)
    const mekanik = await mekanikM.update(payload, id);
    if(mekanik.err || !mekanik.data) {
      return new InternalServerError(`fail to edit mekanik, ${mekanik.err}`)
    }
    delete payload.sandi
    return null

  }

  async delete(id) {
    const mekanik = await mekanikM.delete(id);
    if(mekanik.err || !mekanik.data) {
      return new InternalServerError(`fail to delete mekanik, ${mekanik.err}`)
    }
    return null
  }
}

module.exports = Mekanik