const express = require('express')
const cors = require('cors')
const authRoutes = require('./routes/user_auth')
const indexRouter = require('./routes/index')
const migration = require('../infrastructure/migrations')
const mysqlConn = require('../helper/database/mysql/connection');
const config = require('../config/index');

class AppServer {

  constructor() {
    this.server = express()
    this.server.set('x-powered-by', false)
    this.server.set('etag', false)
    this.server.use(express.urlencoded({ extended: true }))
    this.server.use(express.json())
    this.server.use(cors())
    
    // routing
    this.server.use('/', indexRouter);
    this.server.use('/', authRoutes)

    //db connection
    mysqlConn.createConnectionPool(config.mysqlConfig)

    //db migrations
    migration.init()
  }
}

module.exports = AppServer
