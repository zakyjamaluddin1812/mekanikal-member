const { v4: uuidv4 } = require('uuid')
const Wrapper = require('../../../helper/utils/wrapper')
const MekanikDomain = require('../../../domain/mekanik')
const { get } = require('lodash')
const { database } = require('../../../config')



const wrapper = new Wrapper()
const mekanikD =  new MekanikDomain()

class Mekanik {
    async get(req, res) {
        const query = {...req.query}
        const {data, meta} = await  mekanikD.get(query)
        if (data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'get mekanik success',
            code: 200,
            data,
            meta,
            success: true
        })
    }

    async find(req, res) {
        const id = req.params.id
        const data = await  mekanikD.find(id)
        if (data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'find mekanik success',
            code: 200,
            data,
            success: true
        })
    }

    async update(req, res) {
        const id = req.params.id
        const payload = {...req.body}
        const data = await mekanikD.update(payload, id)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'edit mekanik success',
            code: 200,
            data: payload,
            success: true
        })
    }

    async delete(req, res) {
        const id = req.params.id
        const data = await mekanikD.delete(id)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'delete mekanik success',
            code: 200,
            data : null,
            success: true
        })
    }

    async add(req, res) {
        const payload = { ...req.body }
        const data = await mekanikD.add(payload)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'add layanan success',
            code: 200,
            data: payload,
            success: true
        })
    
    }

}

module.exports = Mekanik