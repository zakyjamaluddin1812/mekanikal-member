const { v4: uuidv4 } = require('uuid')
const Wrapper = require('../../../helper/utils/wrapper')
const StatusMekanikDomain = require('../../../domain/statusMekanik')

const wrapper = new Wrapper()
const statusMekanikD =  new StatusMekanikDomain()

class StatusMekanik {
    async get(req, res) {
        const data = await statusMekanikD.get()
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: `get status mekanik sukses`,
            code: 200,
            data,
            success: true
        })
    }

    async add(req, res) {
        const payload = {...req.body}
        const data = await statusMekanikD.add(payload)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: `add status mekanik sukses`,
            code: 200,
            data: payload,
            success: true
        })
    }
    async update(req, res) {
        const payload = {...req.body}
        const id = req.params.id
        const data = await statusMekanikD.update(payload, id)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: `update status mekanik sukses`,
            code: 200,
            data: payload,
            success: true
        })
    }
    async delete(req, res) {
        const id = req.params.id
        const data = await statusMekanikD.delete(id)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: `delete status mekanik sukses`,
            code: 200,
            data: null,
            success: true
        })
    }
    async find(req, res) {
        const id = req.params.id
        const data = await statusMekanikD.find(id)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: `find status mekanik sukses`,
            code: 200,
            data,
            success: true
        })
    }
}

module.exports = StatusMekanik