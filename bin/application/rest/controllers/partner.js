const { v4: uuidv4 } = require('uuid')
const Wrapper = require('../../../helper/utils/wrapper')
const PartnerDomain = require('../../../domain/partner')
const { get } = require('lodash')
const { database } = require('../../../config')



const wrapper = new Wrapper()
const partnerD =  new PartnerDomain()

class Partner {
    async get(req, res) {
        const paging = {...req.query}
        const {data, meta} = await  partnerD.get(paging)
        if (data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'get partner success',
            code: 200,
            data,
            meta,
            success: true
        })
    }

    async getLokasi(req, res) {
        const data = await  partnerD.getLokasi()
        if (data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'get lokasi partner success',
            code: 200,
            data,
            success: true
        })
    }

    async find(req, res) {
        const id = req.params.id
        const data = await  partnerD.find(id)
        if (data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'find partner success',
            code: 200,
            data,
            success: true
        })
    }

    async update(req, res) {
        const id = req.params.id
        const payload = {...req.body}
        const data = await partnerD.update(payload, id)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'edit partner success',
            code: 200,
            data: payload,
            success: true
        })
    }

    async delete(req, res) {
        const id = req.params.id
        const data = await partnerD.delete(id)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'delete partner success',
            code: 200,
            data : null,
            success: true
        })
    }

    async add(req, res) {
        const payload = { ...req.body }
        const data = await partnerD.add(payload)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: 'add partner success',
            code: 200,
            data: payload,
            success: true
        })
    
    }

}

module.exports = Partner