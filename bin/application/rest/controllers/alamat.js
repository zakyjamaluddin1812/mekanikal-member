const { v4: uuidv4 } = require('uuid')
const Wrapper = require('../../../helper/utils/wrapper')
const AlamatDomain = require('../../../domain/alamat')
const { get } = require('lodash')

const wrapper = new Wrapper()
const alamatD =  new AlamatDomain()

class Alamat {
    async getWilayah (req, res) {
        const kode = req.query.kode
        const {data, message} = await alamatD.getWilayah(kode)
        if(data instanceof Error) return wrapper.responseError(res, data)
        return wrapper.response(res, 200, {
            message: `get ${message} sukses`,
            code: 200,
            data,
            success: true
        })
    }
}

module.exports = Alamat