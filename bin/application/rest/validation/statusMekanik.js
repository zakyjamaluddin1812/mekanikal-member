const Joi = require('joi')
const { UnprocessableEntityError } = require('../../../helper/error')
const Wrapper = require('../../../helper/utils/wrapper')
const wrapper = new Wrapper()

class StatusMekanik {
  params(req, res, next) {
    const schema = Joi.object({
      id : Joi.number().integer()
    })    
    const { error } = schema.validate(req.params, { abortEarly: false })
     if (error) {
      const data = error.details.map(item => {
        const field = item.path[item.path.length - 1]
        return {
          message: item.message.replace(/"/g, ''),
          field
        }
      })
      return wrapper.responseError(res, new UnprocessableEntityError('validation error', data))
    }
    next()
  }

  body(req, res, next) {
    const schema = Joi.object({
      status : Joi.string().required()
    })    
    const { error } = schema.validate(req.body, { abortEarly: false })
     if (error) {
      const data = error.details.map(item => {
        const field = item.path[item.path.length - 1]
        return {
          message: item.message.replace(/"/g, ''),
          field
        }
      })
      return wrapper.responseError(res, new UnprocessableEntityError('validation error', data))
    }
    next()
  }
}

module.exports = StatusMekanik