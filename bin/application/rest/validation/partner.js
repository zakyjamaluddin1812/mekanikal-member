const DateExtension = require('@joi/date')
const JoiImport = require('joi')
const Joi = JoiImport.extend(DateExtension)
const { UnprocessableEntityError } = require('../../../helper/error')
const Wrapper = require('../../../helper/utils/wrapper')
const wrapper = new Wrapper()

class Mekanik {
  body(req, res, next) {
    const schema = Joi.object({
      kodePartner : Joi.string().required(),
      aktif : Joi.boolean().required(),
      namaPartner : Joi.string().required(),
      kepalaPartner : Joi.string().required(),
      alamatLengkap : Joi.string(),
      provinsi : Joi.number().integer(),
      kabupatenKota : Joi.number().integer(),
      kecamatan : Joi.number().integer(),
      kodePos : Joi.number().integer(),
      nomorTelepon : Joi.number().integer(),
      nomorTeleponAlternatif : Joi.number().integer()
    })
    
    const { error } = schema.validate(req.body, { abortEarly: false })
    if (error) {
      const data = error.details.map(item => {
        const field = item.path[item.path.length - 1]
        return {
          message: item.message.replace(/"/g, ''),
          field
        }
      })
      return wrapper.responseError(res, new UnprocessableEntityError('validation error', data))
    }
    next()
  }

  params(req, res, next) {
    const schema = Joi.object({
      id : Joi.number().required()
    })    
    const { error } = schema.validate(req.params, { abortEarly: false })
     if (error) {
      const data = error.details.map(item => {
        const field = item.path[item.path.length - 1]
        return {
          message: item.message.replace(/"/g, ''),
          field
        }
      })
      return wrapper.responseError(res, new UnprocessableEntityError('validation error', data))
    }
    next()
  }

  query(req, res, next) {
    const schema = Joi.object({
      page : Joi.number().required(),
      size : Joi.number().required()
    })    
    const { error } = schema.validate(req.query, { abortEarly: false })
     if (error) {
      const data = error.details.map(item => {
        const field = item.path[item.path.length - 1]
        return {
          message: item.message.replace(/"/g, ''),
          field
        }
      })
      return wrapper.responseError(res, new UnprocessableEntityError('validation error', data))
    }
    next()
  }
}

module.exports = Mekanik