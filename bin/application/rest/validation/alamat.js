const Joi = require('joi')
const { UnprocessableEntityError } = require('../../../helper/error')
const Wrapper = require('../../../helper/utils/wrapper')
const wrapper = new Wrapper()

class Alamat {
  query(req, res, next) {
    const schema = Joi.object({
      kode : Joi.number().integer()
    })    
    const { error } = schema.validate(req.query, { abortEarly: false })
     if (error) {
      const data = error.details.map(item => {
        const field = item.path[item.path.length - 1]
        return {
          message: item.message.replace(/"/g, ''),
          field
        }
      })
      return wrapper.responseError(res, new UnprocessableEntityError('validation error', data))
    }
    next()
  }
}

module.exports = Alamat