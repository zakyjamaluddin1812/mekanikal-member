const router = require('express').Router()
//middleware file
const BasicAuth = require('../rest/authorization/basic_auth')
const BearerAuth = require('../rest/authorization/bearer_auth')
const Mekanik = require('../rest/controllers/mekanik')
const Validation = require('../rest/validation/mekanik')

const basicAuth = new BasicAuth()
const mekanik = new Mekanik()
const validate = new Validation()
const Wrapper = require('../../helper/utils/wrapper')

router.post('/', basicAuth.isAuthenticated, validate.body, mekanik.add)
router.get('/', basicAuth.isAuthenticated, validate.query, mekanik.get)
router.get('/(:id)', basicAuth.isAuthenticated, validate.params, mekanik.find)
router.put('/(:id)', basicAuth.isAuthenticated, validate.params, validate.body, mekanik.update)
router.delete('/(:id)', basicAuth.isAuthenticated, validate.params, mekanik.delete)
//end of router

module.exports = router