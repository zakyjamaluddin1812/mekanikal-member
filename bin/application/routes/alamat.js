const router = require('express').Router()
const BasicAuth = require('../rest/authorization/basic_auth')
const BearerAuth = require('../rest/authorization/bearer_auth')
const AlamatControl = require('../rest/controllers/alamat')
const Validation = require('../rest/validation/alamat')


const validate = new Validation()
const bearerAuth = new BearerAuth()
const basicAuth = new BasicAuth()
const alamatControl = new AlamatControl()

router.get('/', basicAuth.isAuthenticated, validate.query, alamatControl.getWilayah)



module.exports = router