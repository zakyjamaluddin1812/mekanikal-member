const router = require('express').Router()
//middleware file
const BasicAuth = require('../rest/authorization/basic_auth')
const BearerAuth = require('../rest/authorization/bearer_auth')
const Partner = require('../rest/controllers/partner')
const Validation = require('../rest/validation/partner')

const basicAuth = new BasicAuth()
const partner = new Partner()
const validate = new Validation()
const Wrapper = require('../../helper/utils/wrapper')

router.get('/', basicAuth.isAuthenticated, validate.query, partner.get)
router.get('/lokasi', basicAuth.isAuthenticated, partner.getLokasi)
router.post('/', basicAuth.isAuthenticated, validate.body, partner.add)
router.get('/(:id)', basicAuth.isAuthenticated, validate.params, partner.find)
router.put('/(:id)', basicAuth.isAuthenticated, validate.params, validate.body, partner.update)
router.delete('/(:id)', basicAuth.isAuthenticated, validate.params, partner.delete)
//end of router

module.exports = router