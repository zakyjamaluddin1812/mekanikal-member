const router = require('express').Router()
//middleware file
const BasicAuth = require('../rest/authorization/basic_auth')
const BearerAuth = require('../rest/authorization/bearer_auth')
const UserAuthValid = require('../rest/validation/user_auth')
const UserAuthControl = require('../rest/controllers/user_auth')

const basicAuth = new BasicAuth()
const bearerAuth = new BearerAuth()
const userAuthValid = new UserAuthValid()
const userAuthControl = new UserAuthControl()
const Wrapper = require('../../helper/utils/wrapper')
const wrapper = new Wrapper()

//end of middleware

//start router
router.get('/health', (_req, res, _next) => {
  try {
    return wrapper.response(res, 200, {
      message: 'success to get health check data',
      code: 200,
      success: true,
      data: {
        uptime: process.uptime(),
        timestamp: Date.now()
      }
    })
  } catch (err) {
    return wrapper.responseError(res, new ServiceUnavailableError(error.message))
  }
})
router.post('/v1/auth/login', basicAuth.isAuthenticated, userAuthValid.login, userAuthControl.login)
router.post('/v1/auth/register', basicAuth.isAuthenticated, userAuthValid.register, userAuthControl.register)
router.get('/v1/auth/profile', bearerAuth.isAuthenticated, userAuthControl.getUser)
//end of router

module.exports = router