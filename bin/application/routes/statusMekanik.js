const router = require('express').Router()
const BasicAuth = require('../rest/authorization/basic_auth')
const BearerAuth = require('../rest/authorization/bearer_auth')
const StatusMekanikControl = require('../rest/controllers/statusMekanik')
const Validation = require('../rest/validation/statusMekanik')


const validate = new Validation()
const bearerAuth = new BearerAuth()
const basicAuth = new BasicAuth()
const statusMekanikControl = new StatusMekanikControl()

router.get('/', basicAuth.isAuthenticated, statusMekanikControl.get)
router.get('/(:id)', basicAuth.isAuthenticated, validate.params, statusMekanikControl.find)
router.post('/', basicAuth.isAuthenticated, validate.body, statusMekanikControl.add)
router.put('/(:id)', basicAuth.isAuthenticated, validate.params, validate.body, statusMekanikControl.update)
router.delete('/(:id)', basicAuth.isAuthenticated, validate.params, statusMekanikControl.delete)



module.exports = router