const {createClient} = require('redis')
class Redis{
    constructor() {
        // this.client = redisConnect.createClient()
    }
    async connection() {
        try {
            const client = createClient();
            client.on('error', (err) => console.log('Redis Client Error', err));
            await client.connect()
            return client
        } catch (error) {
            console.error(error)
        }
    }
    async setKey(key, value) {
        try {
            const client = await this.connection()
            await client.set(key, value)
        } catch (error) {
            console.error(error)
        }  
    }

    async getKey(key) {
        try {
            const client = await this.connection()
            await client.get(key)
        } catch (error) {
            console.error(error)
        }
    }
    
}

module.exports = Redis