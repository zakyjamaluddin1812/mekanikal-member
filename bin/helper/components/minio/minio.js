const Minio = require('minio');
const { bucketName } = require('../../../infrastructures/global_config');
const config = require('../../../infrastructures/global_config')

let minioClientSdk

const init = () => {
  try {
    minioClientSdk = new Minio.Client(config.minio);
    console.log('minio-client ready');
  } catch (error) {
    console.error(error);
  }
} 

const objectUpload = async (bucketName, objectName, pathFile, metaData = {}) => {
  try {
    const uploadFile = await minioClientSdk.fPutObject(bucketName, objectName, pathFile, metaData)
    return uploadFile
  } catch (error) {
    console.error(error)
    throw new Error('Failed Upload file');
  }
}

const removeObject = async (bucketName, objectName) => {
  try {
    const removeFile = await minioClientSdk.removeObject(bucketName, objectName)
    return console.log('Removed the Object')
  } catch {
    console.error(error)
    throw new Error('Unable to remove object')
  }
}

const makeBucket = async (bucketName, region) => {
  try {
    const makeBucket = await minioClientSdk.makeBucket(bucketName, region)
    return makeBucket
  } catch (error) {
    console.error(error)
    throw new Error('Failed Creating Bucket')
  }
}

const uploadObjectStream = async (bucketName, objectName, stream, size, metaData = {}) => {
  try {
    const uploadFile = await minioClientSdk.putObject(bucketName, objectName, stream, size, metaData)
    return uploadFile
  } catch (error) {
    console.error(error)
    throw new Error ('Failed Upload File Stream')
  }
}


module.exports = {
  init,
  objectUpload,
  removeObject,
  makeBucket,
  uploadObjectStream
}