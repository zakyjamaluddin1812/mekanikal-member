const { mysqlConfig } = require('../../config')
const Wrapper = require('../../helper/utils/wrapper')
const DB = require('../../helper/database/mysql')

const db = new DB(mysqlConfig);
const wrapper = new Wrapper()

class Mekanik {
  async get(skip, size, words, location, status) {
    let sql = `SELECT 
      mekanik.id,
      mekanik.kodeAnggota,
      mekanik.aktif,
      mekanik.namaPengguna,
      mekanik.namaDepan,
      mekanik.namaBelakang,
      mekanik.tanggalLahir,
      mekanik.jenisKelamin,
      mekanik.fotoProfil,
      partner.namaPartner,
      kab.nama AS \`lokasiPartner\`,
      statusMekanik.status AS \`statusMekanik\`,
      mekanik.alamatLengkap AS \`alamat.detail\`,
      provinsi.nama AS \`alamat.provinsi\`,
      kabupatenKota.nama AS \`alamat.kabupaten\`,
      kecamatan.nama AS \`alamat.kecamatan\`,
      mekanik.kodePos AS \`alamat.kodePos\`,
      mekanik.nomorTelepon,
      mekanik.nomorTeleponAlternatif,
      mekanik.email
    FROM mekanik
    LEFT JOIN partner ON partner.id = mekanik.lokasiPartner
    LEFT JOIN kabupatenKota AS kab ON kab.kode = partner.kabupatenKota
    LEFT JOIN statusMekanik ON statusMekanik.id = mekanik.statusMekanik
    LEFT JOIN provinsi ON provinsi.kode = mekanik.provinsi
    LEFT JOIN kabupatenKota ON kabupatenKota.kode = mekanik.kabupatenKota
    LEFT JOIN kecamatan ON kecamatan.kode = mekanik.kecamatan
    WHERE mekanik.deletedAt IS NULL
    `
    const word = words.split(' ')
    const values = []
    word.forEach((item, i) => {
      sql = sql+'AND CONCAT (mekanik.namaPengguna, mekanik.namaDepan, mekanik.namaBelakang, mekanik.nomorTelepon, mekanik.nomorTeleponAlternatif, kab.nama) LIKE ? '
      values.push('%'+word[i]+'%')
    });
    if(location) {
      sql = sql+'AND mekanik.kabupatenKota LIKE ?'
      values.push(location)
    }
    if(status) {
      sql = sql+'AND mekanik.statusMekanik LIKE ?'
      values.push(status)
    }
    sql = sql+'LIMIT ?, ?'
    values.push(skip)
    values.push(size)
    const statement = 'SELECT COUNT(id) AS `count` FROM mekanik  WHERE deletedAt IS NULL'
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      const result2 = await db.query(statement)
      return {
        collection : result,
        totalData : JSON.parse(JSON.stringify(result2.data))[0].count
      }
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async find(id) {
    const sql = `SELECT 
      mekanik.id,
      mekanik.kodeAnggota,
      mekanik.aktif,
      mekanik.namaPengguna,
      mekanik.namaDepan,
      mekanik.namaBelakang,
      mekanik.tanggalLahir,
      mekanik.jenisKelamin,
      mekanik.fotoProfil,
      partner.namaPartner,
      kab.nama AS \`lokasiPartner\`,
      statusMekanik.status AS \`statusMekanik\`,
      mekanik.alamatLengkap AS \`alamat.detail\`,
      provinsi.nama AS \`alamat.provinsi\`,
      kabupatenKota.nama AS \`alamat.kabupaten\`,
      kecamatan.nama AS \`alamat.kecamatan\`,
      mekanik.kodePos AS \`alamat.kodePos\`,
      mekanik.nomorTelepon,
      mekanik.nomorTeleponAlternatif,
      mekanik.email
    FROM mekanik
    LEFT JOIN partner ON partner.id = mekanik.lokasiPartner
    LEFT JOIN kabupatenKota AS kab ON kab.kode = partner.kabupatenKota
    LEFT JOIN statusMekanik ON statusMekanik.id = mekanik.statusMekanik
    LEFT JOIN provinsi ON provinsi.kode = mekanik.provinsi
    LEFT JOIN kabupatenKota ON kabupatenKota.kode = mekanik.kabupatenKota
    LEFT JOIN kecamatan ON kecamatan.kode = mekanik.kecamatan
    WHERE mekanik.deletedAt IS NULL AND mekanik.id = ?`
    const values = [id]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result)
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async add(payload) {
    const {
      namaPengguna, sandi, namaDepan, namaBelakang, tanggalLahir, jenisKelamin,
      aktif, fotoProfil, kodeAnggota, lokasiPartner, statusMekanik, 
      alamatLengkap, provinsi, kabupatenKota, kecamatan, kodePos, 
      nomorTelepon, nomorTeleponAlternatif, email} = payload
    const createdAt = new Date()
    const sql = `INSERT INTO mekanik (
      namaPengguna, sandi, namaDepan, namaBelakang, tanggalLahir, jenisKelamin,
      aktif, fotoProfil, kodeAnggota, lokasiPartner, statusMekanik, 
      alamatLengkap, provinsi, kabupatenKota, kecamatan, kodePos, 
      nomorTelepon, nomorTeleponAlternatif, email, createdAt
    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`
    const values = [
      namaPengguna, sandi, namaDepan, namaBelakang, tanggalLahir, jenisKelamin,
      aktif, fotoProfil, kodeAnggota, lokasiPartner, statusMekanik, 
      alamatLengkap, provinsi, kabupatenKota, kecamatan, kodePos, 
      nomorTelepon, nomorTeleponAlternatif, email, createdAt]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result)
    } catch (err) {
      return wrapper.error(err)
    }
  }
  async update(payload, id) {
    const {
      namaPengguna, sandi, namaDepan, namaBelakang, tanggalLahir, jenisKelamin,
      aktif, fotoProfil, kodeAnggota, lokasiPartner, statusMekanik, 
      alamatLengkap, provinsi, kabupatenKota, kecamatan, kodePos, 
      nomorTelepon, nomorTeleponAlternatif, email} = payload
    const updatedAt = new Date()
    const sql = `UPDATE mekanik SET
    namaPengguna = ?, 
    sandi = ?, 
    namaDepan = ?, 
    namaBelakang = ?, 
    tanggalLahir = ?, 
    jenisKelamin = ?,
    aktif = ?, 
    fotoProfil = ?, 
    kodeAnggota = ?, 
    lokasiPartner = ?, 
    statusMekanik = ?, 
    alamatLengkap = ?, 
    provinsi = ?, 
    kabupatenKota = ?, 
    kecamatan = ?, 
    kodePos = ?, 
    nomorTelepon = ?, 
    nomorTeleponAlternatif = ?, 
    email = ?, 
    updatedAt = ?
    WHERE id = ?`
    const values = [
      namaPengguna, sandi, namaDepan, namaBelakang, tanggalLahir, jenisKelamin,
      aktif, fotoProfil, kodeAnggota, lokasiPartner, statusMekanik, 
      alamatLengkap, provinsi, kabupatenKota, kecamatan, kodePos, 
      nomorTelepon, nomorTeleponAlternatif, email, updatedAt, id]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result)
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async delete(id) {
    const deletedAt = new Date()
    const sql = `UPDATE mekanik SET
                        deletedAt = ?
                        WHERE id = ?`
    const values = [deletedAt, id]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result)
    } catch (err) {
      return wrapper.error(err)
    }
  }
}

module.exports = Mekanik