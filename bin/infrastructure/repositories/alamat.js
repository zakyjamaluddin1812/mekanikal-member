const { mysqlConfig } = require('../../config')
const Wrapper = require('../../helper/utils/wrapper')
const DB = require('../../helper/database/mysql')

const db = new DB(mysqlConfig);
const wrapper = new Wrapper()

class Alamat {

  async getProvinsi() {
    const statement = 'SELECT * FROM wil_provinsi'
    try {
      const result = await db.query(statement)
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async getKabupaten(kode) {
    const statement = 'SELECT * FROM wil_kabupaten_kota WHERE kode_provinsi = ?'
    const data = kode
    try {
      const result = await db.query({sql : statement, values : data})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async getKecamatan(kode) {
    const statement = 'SELECT * FROM wil_kecamatan WHERE kode_kabupaten_kota = ?'
    const data = kode
    try {
      const result = await db.query({sql : statement, values : data})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }
}

module.exports = Alamat