const { mysqlConfig } = require('../../config')
const Wrapper = require('../../helper/utils/wrapper')
const DB = require('../../helper/database/mysql')

const db = new DB(mysqlConfig);
const wrapper = new Wrapper()

class Mekanik {

  async get() {
    const statement = 'SELECT id, status FROM statusMekanik WHERE deletedAt IS NULL'
    try {
      const result = await db.query(statement)
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async find(id) {
    const sql = 'SELECT id, status FROM statusMekanik WHERE deletedAt IS NULL AND id = ?'
    const values = [id]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async add(payload) {
    const {status} = payload
    const createdAt = new Date()
    const sql = 'INSERT INTO statusMekanik (status) VALUES (?)'
    const values = [status, createdAt]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async update(payload, id) {
    const {status} = payload
    const updatedAt = new Date()
    const sql = `UPDATE statusMekanik SET
                        status = ?,
                        updatedAt = ?
                        WHERE id = ?`
    const values = [status, updatedAt, id]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async delete(id) {
    const deletedAt = new Date()
    const sql = `UPDATE statusMekanik SET
                        deletedAt = ?
                        WHERE id = ?`
    const values = [deletedAt, id]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }
}

module.exports = Mekanik