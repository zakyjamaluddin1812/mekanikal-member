const { mysqlConfig } = require('../../config')
const Wrapper = require('../../helper/utils/wrapper')
const DB = require('../../helper/database/mysql')

const db = new DB(mysqlConfig);
const wrapper = new Wrapper()

class Partner {
  async get(skip, size) {
    const sql = `SELECT 
      partner.id,
      partner.kodePartner,
      partner.aktif,
      partner.namaPartner,
      partner.kepalaPartner,
      partner.alamatLengkap AS \`alamat.detail\`,
      provinsi.nama AS \`alamat.provinsi\`,
      kabupatenKota.nama AS \`alamat.kabupaten\`,
      kecamatan.nama AS \`alamat.kecamatan\`,
      partner.kodePos AS \`alamat.kodePos\`,
      partner.nomorTelepon,
      partner.nomorTeleponAlternatif
    FROM partner
    LEFT JOIN provinsi ON provinsi.kode = partner.provinsi
    LEFT JOIN kabupatenKota ON kabupatenKota.kode = partner.kabupatenKota
    LEFT JOIN kecamatan ON kecamatan.kode = partner.kecamatan
    WHERE partner.deletedAt IS NULL
    LIMIT ?, ?`
    const values = [skip, size]
    const statement = 'SELECT COUNT(id) AS `count` FROM partner  WHERE deletedAt IS NULL'
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      const result2 = await db.query(statement)
      return {
        collection : result,
        totalData : JSON.parse(JSON.stringify(result2.data))[0].count
      }
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async getLokasi() {
    const sql = `SELECT
    DISTINCT kabupatenKota.nama,
    kabupatenKota.kode
    FROM partner
    LEFT JOIN kabupatenKota ON kabupatenKota.kode = partner.kabupatenKota
    WHERE partner.deletedAt IS NULL`
    const values = []
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return result
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async find(id) {
    const sql = `SELECT 
      partner.id,
      partner.kodePartner,
      partner.aktif,
      partner.namaPartner,
      partner.kepalaPartner,
      partner.alamatLengkap AS \`alamat.detail\`,
      provinsi.nama AS \`alamat.provinsi\`,
      kabupatenKota.nama AS \`alamat.kabupaten\`,
      kecamatan.nama AS \`alamat.kecamatan\`,
      partner.kodePos AS \`alamat.kodePos\`,
      partner.nomorTelepon,
      partner.nomorTeleponAlternatif
    FROM partner
    LEFT JOIN provinsi ON provinsi.kode = partner.provinsi
    LEFT JOIN kabupatenKota ON kabupatenKota.kode = partner.kabupatenKota
    LEFT JOIN kecamatan ON kecamatan.kode = partner.kecamatan
    WHERE partner.deletedAt IS NULL AND partner.id = ?`
    const values = [id]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async add(payload) {
    const {
      kodePartner, aktif, namaPartner, kepalaPartner,
      alamatLengkap, provinsi, kabupatenKota, kecamatan, kodePos, 
      nomorTelepon, nomorTeleponAlternatif} = payload
    const createdAt = new Date()
    const sql = `INSERT INTO partner (
      kodePartner, aktif, namaPartner, kepalaPartner,
      alamatLengkap, provinsi, kabupatenKota, kecamatan, kodePos, 
      nomorTelepon, nomorTeleponAlternatif, createdAt
    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`
    const values = [
      kodePartner, aktif, namaPartner, kepalaPartner,
      alamatLengkap, provinsi, kabupatenKota, kecamatan, kodePos, 
      nomorTelepon, nomorTeleponAlternatif, createdAt]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }
  async update(payload, id) {
    const {
      kodePartner, aktif, namaPartner, kepalaPartner,
      alamatLengkap, provinsi, kabupatenKota, kecamatan, kodePos, 
      nomorTelepon, nomorTeleponAlternatif} = payload
    const updatedAt = new Date()
    const sql = `UPDATE partner SET
    kodePartner = ?, 
    aktif = ?, 
    namaPartner = ?, 
    kepalaPartner = ?, 
    alamatLengkap = ?, 
    provinsi = ?, 
    kabupatenKota = ?, 
    kecamatan = ?, 
    kodePos = ?, 
    nomorTelepon = ?, 
    nomorTeleponAlternatif = ?, 
    updatedAt = ?
    WHERE id = ?`
    const values = [
      kodePartner, aktif, namaPartner, kepalaPartner,
      alamatLengkap, provinsi, kabupatenKota, kecamatan, kodePos, 
      nomorTelepon, nomorTeleponAlternatif, updatedAt, id]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }

  async delete(id) {
    const deletedAt = new Date()
    const sql = `UPDATE partner SET
                        deletedAt = ?
                        WHERE id = ?`
    const values = [deletedAt, id]
    try {
      const result = await db.query({sql, values})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }
}

module.exports = Partner