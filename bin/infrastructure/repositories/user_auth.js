const { mysqlConfig } = require('../../config')
const Wrapper = require('../../helper/utils/wrapper')
const DB = require('../../helper/database/mysql')

const db = new DB(mysqlConfig);
const wrapper = new Wrapper()

class UserAuth {

  async getUser(username, password) {
    const statement = 'SELECT * FROM users WHERE username = ? AND password = ?'
    const data = [username, password]
    try {
      const result = await db.query(`${statement}, ${data}`)
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {

      return wrapper.error(err.message)
    }
  }

  async getByUsername(username) {
    const statement = `SELECT * FROM users WHERE username = ?`
    const data = [username]
    try {
      const result = await db.query({sql:statement, values : data})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err.message)
    }
  }

  async getByEmail(email) {
    const statement = 'SELECT * FROM users WHERE email = ?'
    const data = [email]
    try {
      const result = await db.query({sql : statement, values : data})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err.message)
    }
  }

  async getByUuid(uuid) {
    const statement = 'SELECT * FROM users WHERE uuid = $1'
    const data = [uuid]
    try {
      const result = await db.query(statement, data)
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err.message)
    }
  }

  async insertUser(uuid, name, username, email, password) {
    // console.log(uuid);

    const statement = 'INSERT INTO users(uuid, name, username, email, password) VALUES(?, ?, ?, ?, ?)'
    const data = [uuid, name, username, email, password]
    try {
      const result = await db.query({sql : statement, values : data})
      if (result.err) throw result.err
      return wrapper.data(result.data)
    } catch (err) {
      return wrapper.error(err)
    }
  }
}

module.exports = UserAuth