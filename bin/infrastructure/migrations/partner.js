const config = require('../../config')
const DB = require('../../helper/database/mysql')
const Logger = require('../../helper/utils/logger')

const logger = new Logger()
let db = null

const createConnectDb = async () => {
  db = new DB(config.mysqlConfig)
}

const migration1 = async () => {
  try {
    if (db) {
      const result = await db.query(`CREATE TABLE IF NOT EXISTS partner(
        id BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            kodePartner varchar(255),
            aktif BIT(1),
            namaPartner varchar(255),
            kepalaPartner varchar(255),
            alamatLengkap text,
            provinsi integer,
            kabupatenKota integer,
            kecamatan integer,
            kodePos integer,
            nomorTelepon varchar(255),
            nomorTeleponAlternatif varchar(255) NULL,
        createdAt TIMESTAMP DEFAULT NOW(),
        updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        deletedAt TIMESTAMP NULL)`)
      if (result.err) throw result.err
    }
  } catch (err) {
    logger.error('partner::migration', 'error on migration db', 'migration1::catch', err)
  }
}

const init = async () => {
  await createConnectDb()
  await migration1()
}

module.exports = init