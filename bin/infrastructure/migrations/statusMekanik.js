const config = require('../../config')
const DB = require('../../helper/database/mysql')
const Logger = require('../../helper/utils/logger')

const logger = new Logger()
let db = null

const createConnectDb = async () => {
  db = new DB(config.mysqlConfig)
}

const migration1 = async () => {
  try {
    if (db) {
      const result = await db.query(`CREATE TABLE IF NOT EXISTS statusMekanik(
        id BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
            status varchar(255),
            createdAt TIMESTAMP DEFAULT NOW(),
            updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            deletedAt TIMESTAMP NULL)`)
      if (result.err) throw result.err
    }
  } catch (err) {
    logger.error('statusMekanik::migration', 'error on migration db', 'migration1::catch', err)
  }
}

const init = async () => {
  await createConnectDb()
  await migration1()
}

module.exports = init