// const userAuthMigration = require('./user_auth')
const mekanikMigration = require('./mekanik')
const provinsiMigration = require('./provinsi')
const kabupatenKotaMigration = require('./kabupatenKota')
const kecamatanMigration = require('./kecamatan')
const statusMekanikMigration = require('./statusMekanik')
const partnerMigration = require('./partner')
const init = () => {
  mekanikMigration()
  provinsiMigration()
  kabupatenKotaMigration()
  kecamatanMigration()
  statusMekanikMigration()
  partnerMigration()
}

module.exports = {
  init
}