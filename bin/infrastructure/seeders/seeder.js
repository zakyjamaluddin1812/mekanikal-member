const provinsi = require("./provinsi")
const kabupatenKota = require("./kabupatenKota")
const kecamatan = require("./kecamatan")
const statusMekanik = require("./statusMekanik")


const seed = async() => {
    await provinsi()
    await kabupatenKota()
    await kecamatan()
    await statusMekanik()
    process.kill(process.pid, 'SIGTERM');
}
seed()



