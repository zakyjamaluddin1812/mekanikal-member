const config = require('../../config')
const DB = require('../../helper/database/mysql')
const Logger = require('../../helper/utils/logger')

const logger = new Logger()
let db = null

const createConnectDb = async () => {
  db = new DB(config.mysqlConfig)
}

const seed = async () => {
  try {
    if (db) {
      const result = await db.query(`INSERT INTO statusMekanik (status) VALUES
      ('freelance');
      `)
      if (result.err) throw result.err
      console.log("seed status mekanik success");
    }
  } catch (err) {
    logger.error('statusMekanik::seed', 'error on migration db', 'seed::catch', err)
  }
}

const init = async () => {
  await createConnectDb()
  await seed()
}

module.exports = init