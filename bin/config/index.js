const packageJson = require('../../package.json')
const dotenv = require('dotenv')

dotenv.config()
const config = {
  server: {
    name: packageJson.name,
    port: process.env.PORT || 3000,
    host: process.env.NODE_ENV === 'production' ? '0.0.0.0' : '127.0.0.1'
  },
  basicAuth: {
    username: process.env.BASIC_AUTH_USERNAME || 'basicAuthUsername',
    password: process.env.BASIC_AUTH_PASSWORD || 'basicAuthPassword'
  },
  bearerAuth: {
    privateKey: process.env.BEARER_AUTH_PRIVATE_KEY || 'bearerAuthPrivateKey',
  },
  database: {
    postgres: {
      url: process.env.POSTGRES_URL || 'postgres://postgres:postgres@localhost:5432/postgres',
    }
  },
  mysqlConfig: {
    connectionLimit: process.env.MYSQL_CONNECTION_LIMIT,
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE
  },
  redisConfig: {
    user: process.env.REDIS_USER || '',
    password: process.env.REDIS_PASSWORD || '',
    host: process.env.REDIS_HOST || 'localhost',
    port: parseInt(process.env.REDIS_PORT | 6379),
  },
}

module.exports = config